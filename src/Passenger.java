public class Passenger {
    private String name;
    private double weight;
    private double relaxationWeight = 0.1;
    private int limit=15;
    public Passenger(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public void calculateBill() {
        if (weight <= limit) {
            System.out.println("No extra charges");
        } else {
            try {
                double extraWeight = weight - limit;
                if (extraWeight <= relaxationWeight)
                    throw new RelaxationException();
                else
                    System.out.println("Additional charges to be paid Rs." + (500 * extraWeight));
            } catch (RelaxationException e) {
                System.out.println(e.getMessage());
                System.out.println();
            }
        }
    }
}
